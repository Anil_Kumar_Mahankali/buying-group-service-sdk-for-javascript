import {inject} from 'aurelia-dependency-injection';
import BuyingGroupServiceSdkConfig from './buyingGroupServiceSdkConfig';
import BuyingGroupSynopsisView from './buyingGroupSynopsisView';
import {HttpClient} from 'aurelia-http-client';

@inject(BuyingGroupServiceSdkConfig,HttpClient)
class ListBuyingGroupsFeature{

    _config:BuyingGroupServiceSdkConfig;
    _httpClient:HttpClient;

    constructor(config:BuyingGroupServiceSdkConfig,
                httpClient:HttpClient){
        if(!config){
            throw 'config required';
        }
        this._config=config;

        if(!httpClient){
            throw 'httpClient required';
        }
        this._httpClient=httpClient;
    }

    /**
     * list Buying Groups
     * @param {string} accessToken
     */
    execute(accessToken:string):Promise<BuyingGroupSynopsisView[]>
    {
        return this._httpClient
            .createRequest(`/buying-groups`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => response);
    }

}

export  default ListBuyingGroupsFeature;