import DiContainer from './diContainer';
import BuyingGroupServiceSdkConfig from './buyingGroupServiceSdkConfig';
import ListBuyingGroupsFeature from './listBuyingGroupsFeature';

/**
 * @class {BuyingGroupServiceSdk}
 */

export default class BuyingGroupServiceSdk{

    _diContainer:DiContainer;

    constructor(config:BuyingGroupServiceSdkConfig) {
       this._diContainer =  new DiContainer(config)
    }

    listBuyingGroups(accessToken:string):Promise<Array> {
        return this
            ._diContainer
            .get(ListBuyingGroupsFeature)
            .execute(accessToken)
    }

}
