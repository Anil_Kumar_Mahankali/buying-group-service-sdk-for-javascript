import BuyingGroupServiceSdk,
{
BuyingGroupSynopsisView
} from  '../../src/index';
import  factory from './factory';
import config from './config';
//import dummy from '../dummy';

/*
tests
 */

describe('Index module', () => {


    describe('default export', () => {
        it('should be BuyingGroupServiceSdk constructor', () => {
            /*
             act
             */
            const objectUnderTest =
                new BuyingGroupServiceSdk(
                    config.buyingGroupServiceSdkConfig
                );

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(BuyingGroupServiceSdk));

        });
    });

        describe('listBuyingGroups method', () => {
                it('should return more than 1 result', (done) => {
                    /*
                     arrange
                     */
                    const objectUnderTest =
                        new BuyingGroupServiceSdk(
                            config.buyingGroupServiceSdkConfig
                        );


                    /*
                     act
                     */
                    const listBuyingGroupsSynopsisViewsPromise =
                        objectUnderTest
                            .listBuyingGroups(
                                factory.constructValidAppAccessToken()
                            );

                    /*
                     assert
                     */
                    listBuyingGroupsSynopsisViewsPromise
                        .then((BuyingGroupSynopsisView) => {
                            expect(BuyingGroupSynopsisView).toBeTruthy();
                            done();
                        })
                        .catch(error=> done.fail(JSON.stringify(error)));

                }, 20000);
            });

    });