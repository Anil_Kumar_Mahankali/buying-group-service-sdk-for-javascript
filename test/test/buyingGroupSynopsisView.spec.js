import BuyingGroupSynopsisView from '../../src/buyingGroupSynopsisView';
import dummy from '../dummy';

describe('BuyingGroupSynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new BuyingGroupSynopsisView(null, dummy.name);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = "1";

            /*
             act
             */
            const objectUnderTest =
                new BuyingGroupSynopsisView(expectedId, dummy.name);

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new BuyingGroupSynopsisView(dummy.id, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });

        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.name;

            /*
             act
             */
            const objectUnderTest =
                new BuyingGroupSynopsisView(dummy.id, expectedName);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
    })
});

